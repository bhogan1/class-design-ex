#include <iostream>
#include <string>
using namespace std;

class State{
    public:
        //Constructors
        State();
        State(string, string, string, int, int, int);
        //Access Methods
        string getAbrev();
        string getName();
        string getCap();
        int getPop();
        int getYear();
        int getReps();

        void setAbrev(string);
        void setName(string);
        void setCap(string);
        void setPop(int);
        void setYear(int);
        void setReps(int);
        //Utility Methods
        void printFull();
        void printPop();
        void printReps();
        void printYear();
    private:
        string abrev;
        string name;
        string cap;
        int pop;
        int year;
        int reps;
};
