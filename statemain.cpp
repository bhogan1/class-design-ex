#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "state.h"
using namespace std;

void setup(vector<State> &); //Loads in data from a file
void menu(); //Displays menu of options
void findAbv(vector<State> &); //Finds states with a specific abbreviation
void minPop(vector<State> &); //Finds states with at least x people
void maxPop(vector<State> &); //Finds states with at most x people
void findReps(vector<State> &); //Finds states with x representatives
void beforeYear(vector<State> &); //Finds states founded beofre x
void afterYear(vector<State> &); //Finds states founded after x
void displayall(vector<State> &); //Prints all states data in full

int main(){
    vector<State> states;
    setup(states);
    int op, run = 1;

    while(run == 1){
        menu();
        cout << "Enter an option: ";
        cin >> op; //User entry controls switch branching
        cout << endl;

        switch(op){ //Switch statement sends to functions
            case 1:
                findAbv(states);
                break;
            case 2:
                minPop(states);
                break;
            case 3:
                maxPop(states);
                break;
            case 4:
                findReps(states);
                break;
            case 5:
                beforeYear(states);
                break;
            case 6:
                afterYear(states);
                break;
            case 7:
                displayall(states);
                break;
            case 8: 
                run = 0; // Entering 8 terminates the program
        }
    }
    cout << "Thank you for using the program!" << endl;
    return 0;
}

void setup(vector<State> & states){ // Loads in data to the vector from the file
    string file;
    cout << "Enter a file name: ";
    getline(cin,file); // Gets user input for file name
    cout << endl;
    ifstream infile;
    infile.open(file);

    while(!infile){ // If the file doesn't exist, prompts the user for more input
        cout << "Enter a valid file name: ";
        getline(cin,file);
        cout << endl;
        infile.open(file);
    }

    State st; // variable for a single state
    string abrev, name, cap, pop_s, year_s, reps_s; // variables to read in the 6 items in a row
    int pop, year, reps; // the int version of the pop, year, and reps
    char c = infile.peek();
    while(c != EOF) { // Using the known file format, takes in 6 data per line
        getline(infile, abrev, ',');
        getline(infile, name, ',');
        getline(infile, cap, ',');
        getline(infile, pop_s, ',');
        getline(infile, year_s, ',');
        getline(infile, reps_s, '\n'); // 6th data will be followed by endline
        pop = stoi(pop_s); // convert to integer 
        year = stoi(year_s); // convert to integer 
        reps = stoi(reps_s); // convert to integer 
        st = State(abrev, name, cap, pop, year, reps);  // create a state object with our values
        states.push_back(st); // add to the data structure
        c = infile.peek(); // Checks to see if the file will end or if there are more lines
    }
    infile.close();
}

void menu(){
    cout << "---Program Menu---" << endl;
    cout << "------------------" << endl;
    cout << "1) Search state based on abbreviation" << endl;
    cout << "2) Search state based on minimum population" << endl;
    cout << "3) Search state based on maximum population" << endl;
    cout << "4) Search state based on representatives" << endl;
    cout << "5) Search state based on if was founded before a year" << endl;
    cout << "6) Search state based on if was founded after a year" << endl;
    cout << "7) Display all state information" << endl;
    cout << "8) Quit the program" << endl << endl;
}

void findAbv(vector<State> &states){
    string line; int found = 0;
    cin.ignore();
    cout << "Enter abbreviation to search for: ";
    getline(cin,line);
    cout << endl;
    for(auto it = line.begin(); it < line.end(); it++)
        *it = tolower(*it); //Converts user input to all lowercase
    for(auto it = states.begin(); it < states.end(); it++){
        string abrevL, abrev; // Declares fresh strings for each new entry
        abrev = (*it).getAbrev();
        for(auto jt = abrev.begin(); jt < abrev.end(); jt++)
            abrevL.push_back(tolower(*jt)); //Converts actual abbreviation to lowercase for comparison
        if(abrevL.find(line) != string::npos){ //Case insensitive search
            (*it).printFull();
            cout << endl;
            found = 1;
            break; //Each abbreviation is unique
        }
    }
    if(found == 0) //If abbreviation is not found display special message
        cout << "Abbreviation not found in state list." << endl << endl;
}

void minPop(vector<State> &states){
    int pop, count = 0;
    cout << "Enter a minimum population to search for: ";
    cin >> pop;
    cout << endl;
    for(auto it = states.begin(); it < states.end(); it++)
        if(pop <= (*it).getPop()){
            (*it).printPop();
            count++;
        }
    cout << "------------------" << endl;
    cout << "In total, " << count << " states have a population above " << pop << "." << endl << endl;
}

void maxPop(vector<State> &states){
    int pop, count = 0;
    cout << "Enter a maximum population to search for: ";
    cin >> pop;
    cout << endl;
    for(auto it = states.begin(); it < states.end(); it++)
        if(pop >= (*it).getPop()){
            (*it).printPop();
            count++;
        }
    cout << "------------------" << endl;
    cout << "In total, " << count << " states have a population below " << pop << "." << endl << endl;
}

void findReps(vector<State> &states){
    int reps, count = 0;
    cout << "Enter the number of representatives to search for: ";
    cin >> reps;
    cout << endl;
    for(auto it = states.begin(); it < states.end(); it++)
        if(reps == (*it).getReps()){
            (*it).printReps();
            count++;
        }
    cout << "------------------" << endl;
    cout << "In total, " << count << " states have " << reps << " representatives." << endl << endl;
}

void beforeYear(vector<State> &states){
    int year, count = 0;
    cout << "Enter the year to search states founded before: ";
    cin >> year;
    cout << endl;
    for(auto it = states.begin(); it < states.end(); it++)
        if(year >= (*it).getYear()){
            (*it).printYear();
            count++;
        }
    cout << "------------------" << endl;
    cout << "In total, " << count << " states were founded before " << year << endl << endl;
}

void afterYear(vector<State> &states){
    int year, count = 0;
    cout << "Enter the year to search states founded after: ";
    cin >> year;
    cout << endl;
    for(auto it = states.begin(); it < states.end(); it++)
        if(year <= (*it).getYear()){
            (*it).printYear();
            count++;
        }
    cout << "------------------" << endl;
    cout << "In total, " << count << " states were founded after " << year << endl << endl;
}

void displayall(vector<State> &states){
    for (auto it = states.begin(); it < states.end(); it++) {
        (*it).printFull();
        cout << endl;
    }
}
