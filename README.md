This is an example file while includes a makefile along with a state class

Notes: 
Make the executable by just typing "make"
runstate is your executable, run that file with ./runstate and follow the commands
usstates.csv is the file you want to feed your program to have all the necessary input

File Organization:
usstates.csv: Your input file. This has all the information separated by new lines and commas
state.h: The class header file
state.cpp: The class implementation
statemain.cpp: The main file which utilizes the class