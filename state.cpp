#include "state.h"
#include <iostream>
#include <string>
using namespace std;

//Default constructor
State::State(){
}

//Alternate constructor
State::State(string a, string n, string c, int p, int y, int r){
    abrev  = a; 
    name   = n; 
    cap    = c; 
    pop    = p; 
    year   = y; 
    reps   = r;
}

//Get and set methods
string State::getAbrev(){
    return abrev;
}

string State::getName(){
    return name;
}

string State::getCap(){
    return cap;
}

int State::getPop(){
    return pop;
}

int State::getYear(){
    return year;
}

int State::getReps(){
    return reps;
}

void State::setAbrev(string a){
    abrev = a;
}

void State::setName(string n){
    name = n;
}

void State::setCap(string c){
    cap = c;
}

void State::setPop(int p){
    pop = p;
}

void State::setYear(int y){
    year = y;
}

void State::setReps(int r){
    reps = r;
}

//Special print methods
void State::printFull(){
    cout << "State name: " << name << endl;
    cout << "Abbreviation: " << abrev << endl;
    cout << "Capital: " << cap << endl;
    cout << "Population: " << pop << endl;
    cout << "Year founded: " << year << endl;
    cout << "Representatives in the House: " << reps << endl;
}

void State::printPop(){
    cout << name << " has a population of " << pop << endl;
}

void State::printReps(){
    cout << name << " has " << reps << " representatives" << endl;
}

void State::printYear(){
    cout << name << " was founded in " << year << endl;
}
